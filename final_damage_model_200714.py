
"""
Title: Bacterial respiration during stationary phase induces intracellular damage that leads to delayed regrowth

Authors: Spencer Cesar, Lisa Willis, Kerwyn Casey Huang.

Python 3.0 code for Figure 5D
"""

import numpy as np
import matplotlib.pyplot as plt


N = 1000 #Number of cells (fixed)
A1DivA2 = [0.7]  #A_1/A_2
div_death = [0.15] # rho * A_1/delta

times_d = np.array([12.0,14.0,16.0,18.0,20.0,22.0,24.0,36.0])
scal_times = times_d -11.0
scal_times =  scal_times/scal_times[-1]
growing = [0.98, 0.92, 0.41, 0.17, 0.11, 0.12, 0.13, 0.056]
delayed = [0.00, 0.06, 0.49, 0.37, 0.09, 0.05, 0.051, 0.024]
non_growing = [0.01, 0.01, 0.06, 0.41, 0.78, 0.80, 0.80, 0.90]

#Figure 5D: gamma A1/delta = 0.77, rho * A1/delta =0.15, A1/A2 = 0.7, A2 = 25

A_2 = 25
delta = A_2

for k in range(len(A1DivA2)): #create grid of A_1/A_2 x rho A_1/delta if you want
   for j in range(len(div_death)):
      
      delta = A_2
      N_events = 3600*A_2
      A_1 = int(np.floor(A1DivA2[k]*A_2))
      rho = div_death[j] * delta/A_1
      gamma = 0.77 *delta/A_1

      print "rho, gamma, A_1, A_2 = ", rho, gamma, A_1, A_2
      
      cells = [0] * N #Initally all cells have zero damage
      cells_t = [cells[:]]
      times = [0]
      T_tot = 0
      count_t = 0
      for i in range(1,N_events): 
         #sum total rate of events
         rate = delta*N #all cells accumulate damage at rate delta
         for cell in cells: 
            #rate = rate + rho*max(alpha*(0.5-cell/float(A_no))+1,0) 
            if cell >= A_2: 
                rate = rate + gamma #cells with large damage die
            if cell != -1:
                rate = rate + rho #cells that are not dead/dormant divide
         #time of next event
         T_next = np.random.exponential(1.0/rate)
         T_tot = T_tot + T_next 
         
         #next event
         rand = rate*np.random.uniform(0,1)
         sum_cells = 0
         count = 0 #index of cell to which event happens
         while sum_cells < rand:
            sum_cells = sum_cells + delta #damage
            if cells[count] >= A_2:
               sum_cells = sum_cells + gamma #dormant
            if cells[count] != -1:
               sum_cells = sum_cells + rho #division if not dormant                   
            count = count + 1          
         count = count -1    
         
         if sum_cells - rand < delta : #event is damage
            if cells[count]!= -1 and cells[count] < A_2: 
                cells[count] = cells[count]+1 #add damage if cell is not dead or non-growing           
         elif (cells[count] >= A_2 and sum_cells - rand < delta + gamma): #strongly damaged cells die
            cells[count] = -1         
             
         else:      #cell divides, replacing a randomly choosen cell that dies with a daughter cell with 0 damage
            rand_indx2 = np.random.randint(0,N)
            cells[rand_indx2] = 0  

         if i%(N_events/50)==0: 
            cells_t.append(cells[:])
            print i
            times.append(T_tot)
            
      plt.figure(figsize = (5.7,2.5)) 
      plt.plot(times, [np.sum([c_t.count(ss) for ss in range(0, A_1)])/float(N) for c_t in cells_t], 'green') #sum number of growing cells
      plt.plot(times, [(c_t.count(-1) + c_t.count(A_2))/float(N) for c_t in cells_t], 'black') #sum number of non-growing cells
      plt.plot(times, [np.sum([c_t.count(ss) for ss in range(A_1, A_2)])/float(N) for c_t in cells_t], 'red') #sum number of delayed cells
      plt.plot(times, [c_t.count(-1)/float(N) for c_t in cells_t], 'purple') #sum number of dead cells
      plt.plot(scal_times*times[-1], growing, 'o', color='green')
      plt.plot(scal_times*times[-1], delayed, 'o', color='red')
      plt.plot(scal_times*times[-1], non_growing,'o',color= 'black')
      plt.savefig("figure_5D.pdf", format='pdf', dpi=200,bbox_inches='tight')
      
 
